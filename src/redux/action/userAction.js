import { SET_USER_LOGIN, SET_USER_SIGNUP } from "../constant/userConstant";
import { userServ } from "../../service/userService";
import { localLoginServ, localUserServ } from "../../service/localService";

export const setLoginActionService = (values, onSuccess) => {
  // values đến từ form của antd
  return (dispatch) => {
    userServ
      .login(values)
      .then((res) => {
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
        localUserServ.set(res.data.content);
        onSuccess();
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
export const setSignUp = (values, onSuccess) => {
  // values đến từ form của antd
  return (dispatch) => {
    userServ
      .signup(values)
      .then((res) => {
        dispatch({
          type: SET_USER_SIGNUP,
          payload: res.data.content,
        });
        localLoginServ.get(res.data.content);
        onSuccess();
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
