import React from "react";
import { NavLink } from "react-router-dom";
import UserMenu from "./UserMenu";

export default function Header() {
  return (
    <div className="h-20 shadow fixed top-0 left-0 right-0 z-40 bg-white ">
      <div className="container mx-auto flex justify-between items-center h-full  ">
        <NavLink to="/">
          <img
            src="http://demo1.cybersoft.edu.vn/logo.png"
            alt=""
            width={150}
            height={70}
          />
        </NavLink>
        <div>
          <NavLink>
            <span className="mx-2 font-medium">Lịch Chiếu</span>
          </NavLink>
          <NavLink>
            <span className="mx-2 font-medium">Tin Tức</span>
          </NavLink>
          <NavLink>
            <span className="mx-2 font-medium">Ứng Dụng</span>
          </NavLink>
        </div>
        <UserMenu />
      </div>
    </div>
  );
}
