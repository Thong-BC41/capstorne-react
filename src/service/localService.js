export const USER_LOGIN = "USER_LOGIN";
export const USER_SIGNUP = "USER_SIGNUP";

export const localUserServ = {
  get: () => {
    let jsonData = localStorage.getItem(USER_LOGIN);
    return JSON.parse(jsonData);
  },

  set: (userInfo) => {
    let jsonData = JSON.stringify(userInfo);
    localStorage.setItem(USER_LOGIN, jsonData);
  },
  remove: () => {
    localStorage.removeItem(USER_LOGIN);
  },
};
export const localLoginServ = {
  get: () => {
    let jsonData = localStorage.getItem(USER_SIGNUP);
    return JSON.parse(jsonData);
  },
};
