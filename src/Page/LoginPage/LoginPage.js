import { Button, Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import Lottie from "lottie-react";
import login_animate from "../../asset/animte_login.json";
import { setLoginActionService } from "../../redux/action/userAction";
const LoginPage = () => {
  let navigate = useNavigate();
  let dispatch = useDispatch();

  const onFinishThunk = (values) => {
    let handleSuccess = () => {
      message.success("Đăng nhập thành công");
      navigate("/");
    };
    dispatch(setLoginActionService(values, handleSuccess));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="w-screen h-screen p-20 bg-[url(http://demo1.cybersoft.edu.vn/static/media/backapp.b46ef3a1.jpg)] flex justify-center items-center">
      <div className="container  p-20  bg-white rounded-lg flex ">
        <div className="w-1/2 h-full ">
          <Lottie animationData={login_animate} loop={true} />
        </div>
        <div className="w-1/2 h-full ">
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            className="w-full"
            initialValues={{
              remember: true,
            }}
            onFinish={onFinishThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 0,
                span: 24,
              }}
            >
              <Button danger type="primary" htmlType="submit" className="mr-10">
                Submit
              </Button>
              <NavLink className="underline" to="/signup">
                Chưa có tài khoản : đăng ký
              </NavLink>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
