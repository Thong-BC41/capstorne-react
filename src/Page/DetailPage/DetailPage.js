import { Progress, Tabs } from "antd";
import React, { useEffect, useState } from "react";
import { NavLink, redirect, useParams } from "react-router-dom";
import { movieServ } from "../../service/movieService";
import ReactPlayer from "react-player";
import ItemDetail from "./ItemDetail";
import ItemMovie from "../HomePage/ListMovie/ItemMovie";
import ItemTabsMovie from "../HomePage/TabsMovie/ItemTabsMovie";

const onChange = (key) => {
  console.log(key);
};

export default function DetailPage() {
  let params = useParams();
  const [movie, setMovie] = useState([]);
  useEffect(() => {
    let fetchDetail = async () => {
      try {
        let result = await movieServ.getItemDetail(params.id);
        setMovie(result.data.content);

        //
      } catch (error) {
        console.log(error);
      }
    };
    fetchDetail();
  }, []);

  const [danhSachHeThongRap, setDanhSachHeThongRap] = useState([]);
  useEffect(() => {
    movieServ
      .getMovieByTheater()
      .then((res) => {
        setDanhSachHeThongRap(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderHeThongRap = () => {
    return danhSachHeThongRap.map((heThongRap) => {
      console.log(danhSachHeThongRap);
      return {
        key: heThongRap.maHeThongRap,
        label: <img className="h-16" src={heThongRap.logo} alt="" />,
        children: (
          <Tabs
            style={{ height: 700 }}
            tabPosition="left"
            defaultActiveKey="1"
            items={heThongRap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.tenCumRap,
                label: (
                  <div className="w-60 truncate  ">
                    <p className="font-medium">{cumRap.tenCumRap}</p>
                    <p className="text-xs text-gray-600">{cumRap.diaChi}</p>
                  </div>
                ),
                children: (
                  <div
                    style={{ height: 700, overflowY: "scroll" }}
                    className="space-y-5"
                  >
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemTabsMovie phim={phim} />;
                    })}
                  </div>
                ),
              };
            })}
            onChange={onChange}
          />
        ),
      };
    });
  };

  return (
    <div className="container   ">
      <div className="flex items-stretch mb-5">
        <img className="w-1/4" src={movie.hinhAnh} alt="" />
        <div className="p-5 space-y-10">
          <h2 className="text-xl font-medium">{movie.tenPhim}</h2>
          <p className="text-xs text-gray-600">{movie.moTa}</p>
          <Progress
            className="pr-3 my-3"
            strokeColor={{
              "0%": "#108ee9",
              "100%": "#87d068",
            }}
            format={(percent) => `${percent / 10} Điểm`}
            type="circle"
            percent={movie.danhGia * 10}
          />
          <br />
          <NavLink
            to={`/booking/${movie.maPhim}`}
            className="my-5 px-5 py-2 rounded bg-red-500 text-white"
          >
            Mua vé
          </NavLink>
          <br />
          <br />
          <NavLink
            to={`/`}
            className="my-5 px-5 py-2 rounded bg-red-500 text-white"
          >
            Trở về trang chủ
          </NavLink>
        </div>
        <div name="trailer">
          <p className="text-red-500 text-xl py-2">XEM TRAILER :</p>
          <ReactPlayer url={movie.trailer} />
        </div>
      </div>
      <div>
        <Tabs
          style={{ height: 700 }}
          tabPosition="left"
          defaultActiveKey="1"
          items={renderHeThongRap()}
          onChange={onChange}
        />
      </div>
    </div>
  );
}
