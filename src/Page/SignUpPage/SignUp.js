import React, { useState } from "react";
import { Form, Button, Input, message } from "antd";
import Lottie from "lottie-react";
import signup_animate from "../../asset/animte_signup.json";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setSignUp } from "../../redux/action/userAction";
import axios from "axios";
import {
  checkEmail,
  checkFullName,
  checkPassword,
  checkPhoneVietNam,
  repeatPassword,
} from "./ValidationForm";

const SignUp = () => {
  let navigate = useNavigate();
  let dispatch = useDispatch();

  const onFinishSignUp = async (values) => {
    let total = { ...values, maNhom: "GP05" };
    let { taiKhoan, matKhau, confirmpassword, hoTen, email, soDt } = total;
    let fetchSignUp = async () => {
      let isValidation =
        checkPassword(matKhau) &&
        repeatPassword(matKhau, confirmpassword) &&
        checkFullName(hoTen) &&
        checkEmail(email) &&
        checkPhoneVietNam(soDt);
      if (isValidation) {
        try {
          let handleSignUp = () => {
            message.success("Đăng ký thành công");
            navigate("/login");
          };
          console.log(values);
          dispatch(setSignUp(values, handleSignUp));
        } catch (error) {
          message.success(error.data.content);
        }
      }
    };
    fetchSignUp();
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="w-screen h-screen p-20 bg-[url(http://demo1.cybersoft.edu.vn/static/media/backapp.b46ef3a1.jpg)] flex justify-center items-center">
      <div className="container  p-20 bg-slate-300 rounded-lg flex ">
        <div className="w-1/2 h-full ">
          <Lottie animationData={signup_animate} loop={true} />
        </div>
        <div className="p-150 w-1/2 h-full ">
          <h2 className="py-4">Nhập thông tin đăng ký :</h2>
          <Form onFinish={onFinishSignUp} onFinishFailed={onFinishFailed}>
            <label
              for="UserName"
              className="mb-1 text-xs sm:text-sm tracking-wide text-gray-600"
            >
              User:
            </label>
            <Form.Item
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Nhập tài khoản",
                },
              ]}
            >
              <Input placeholder="Username" />
            </Form.Item>
            <label
              for="UserName"
              className="mb-1 text-xs sm:text-sm tracking-wide text-gray-600"
            >
              password:
            </label>
            <Form.Item
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Nhập mật khẩu",
                },
              ]}
            >
              <Input placeholder="password" />
            </Form.Item>
            <label
              for="UserName"
              className="mb-1 text-xs sm:text-sm tracking-wide text-gray-600"
            >
              Confirmpassword:
            </label>
            <Form.Item
              name="confirmpassword"
              rules={[
                {
                  required: true,
                  message: "Mật khẩu không khớp",
                },
              ]}
            >
              <Input placeholder="Confirm the password" />
            </Form.Item>
            <label
              for="UserName"
              className="mb-1 text-xs sm:text-sm tracking-wide text-gray-600"
            >
              Email:
            </label>
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: "Nhập Email",
                },
              ]}
            >
              <Input placeholder="email" />
            </Form.Item>
            <label
              for="UserName"
              className="mb-1 text-xs sm:text-sm tracking-wide text-gray-600"
            >
              Phone:
            </label>
            <Form.Item
              name="soDt"
              rules={[
                {
                  required: true,
                  message: "Nhập số điện thoại",
                },
              ]}
            >
              <Input placeholder="Phone" />
            </Form.Item>
            <label
              for="UserName"
              className="mb-1 text-xs sm:text-sm tracking-wide text-gray-600"
            >
              Team:
            </label>
            <Form.Item
              name="maNhom"
              rules={[
                {
                  required: true,
                  message: "Nhập mã nhóm",
                },
              ]}
            >
              <Input placeholder="Team" />
            </Form.Item>
            <label
              for="UserName"
              className="mb-1 text-xs sm:text-sm tracking-wide text-gray-600"
            >
              Username:
            </label>
            <Form.Item
              name="hoTen"
              rules={[
                {
                  required: true,
                  message: "Nhập họ tên",
                },
              ]}
            >
              <Input placeholder="name" />
            </Form.Item>

            <Button danger type="primary" htmlType="submit" className="mr-5">
              submit
            </Button>
            <NavLink to="/login" className="text-red-900 font-medium">
              Về trang đăng nhập
            </NavLink>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
