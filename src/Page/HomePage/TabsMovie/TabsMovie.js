import React, { useEffect, useState } from "react";
import { movieServ } from "../../../service/movieService";
import { Tabs } from "antd";
import ItemTabsMovie from "./ItemTabsMovie";
const onChange = (key) => {
  console.log(key);
};

export default function TabsMovie() {
  const [danhSachHeThongRap, setDanhSachHeThongRap] = useState([]);
  useEffect(() => {
    movieServ
      .getMovieByTheater()
      .then((res) => {
        setDanhSachHeThongRap(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderHeThongRap = () => {
    return danhSachHeThongRap.map((heThongRap) => {
      console.log(danhSachHeThongRap);
      return {
        key: heThongRap.maHeThongRap,
        label: <img className="h-16" src={heThongRap.logo} alt="" />,
        children: (
          <Tabs
            style={{ height: 700 }}
            tabPosition="left"
            defaultActiveKey="1"
            items={heThongRap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.tenCumRap,
                label: (
                  <div className="w-60 truncate  ">
                    <p className="font-medium">{cumRap.tenCumRap}</p>
                    <p className="text-xs text-gray-600">{cumRap.diaChi}</p>
                  </div>
                ),
                children: (
                  <div
                    style={{ height: 700, overflowY: "scroll" }}
                    className="space-y-5"
                  >
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemTabsMovie phim={phim} />;
                    })}
                  </div>
                ),
              };
            })}
            onChange={onChange}
          />
        ),
      };
    });
  };
  return (
    <div className="container">
      <Tabs
        style={{ height: 700 }}
        tabPosition="left"
        defaultActiveKey="1"
        items={renderHeThongRap()}
        onChange={onChange}
      />
    </div>
  );
}
