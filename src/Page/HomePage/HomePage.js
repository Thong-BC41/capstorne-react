import React from "react";
import ListMovie from "./ListMovie/ListMovie";
import TabsMovie from "./TabsMovie/TabsMovie";
import Banner from "./Banner/Banner";

export default function HomePage() {
  return (
    <div className="space-y-3">
      <Banner />
      <ListMovie />
      <br />
      <TabsMovie />
    </div>
  );
}
