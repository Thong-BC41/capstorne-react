import { Carousel } from "antd";

const Banner = () => {
  return (
    <Carousel autoplay>
      <div>
        <img
          style={{
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
          }}
          src="https://movienew.cybersoft.edu.vn/hinhanh/ban-tay-diet-quy.png"
          alt=""
        />
      </div>
      <div>
        <img
          style={{
            width: "100%",
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
          }}
          className="bg-cover"
          src="https://movienew.cybersoft.edu.vn/hinhanh/lat-mat-48h.png"
          alt=""
        />
      </div>
      <div>
        <img
          style={{
            width: "100%",
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
          }}
          className="bg-cover"
          src="https://movienew.cybersoft.edu.vn/hinhanh/cuoc-chien-sinh-tu.png"
          alt=""
        />
      </div>
    </Carousel>
  );
};

export default Banner;
