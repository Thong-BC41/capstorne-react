import React from "react";
import Footer from "../Components/Footer/Footer";
import Header from "../Components/Header/Header";

export default function Layout({ Component }) {
  return (
    <div className="space-y-10 h-full min-h-screen flex flex-col relative z-50">
      <Header />
      <div className="flex-grow">
        <Component />
      </div>
      <Footer />
    </div>
  );
}
